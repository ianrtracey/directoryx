Quick Start:
====================
1. Install neo4j http://neo4j.com/download/
2. Install neo4j to /server at your computer's root directory (if /server is not already created - create create it with 'sudo mkdir /servers')
3. Rename the downloaded neo4j folder to 'neo4j'
4. cd into directoryx/scripts and run 'ruby gen_data.rb' to generate sample data
5. cd into directoryx/importer and run 'sh import-mvn.sh' to batch import to the database (you may receive warnings - you can ignore these).
6. Start the neo4j remote server '/servers/neo4j/bin ./neo4j start' (stop with '/servers/neo4j/bin ./neo4j stop'
7. Run the app using 'mvn spring-boot:run' and go to localhost:8080
8. You can also view your neo4j nodes and connections at localhost:7474/browser

Introduction
====================

A project to build a next generation directory **web application** that is mobile-ready, responsive, and 
focused on the relationships of a typical **_business network_**. 

* People
* Projects
* Teams
* Places

We plan to use a graph database (Neo4j) to store the objects and relationships, 
so the system can scale to literally billions of nodes and connections. 

Key features
-----------

* Search 
    * Auto-completion for userid/username
	* Soundex matching
	* Project name and description search
* Dashboard that shows the currently logged in users connections.
    * People: Current, Peers, Reports (if present), Manager
	* Places: Current, Favorites
	* Teams: active teams
	* Projects: active projects, link to the archives.
* Web Service REST API
	* Views of the data objects
	* Updating and adding new network objects.
* Detail Views
	* Details of a person
	* Details for a group of people
	* Details of a Project, Place, Team
* Browsing
	* Visualization (ala D3JS.org) of a group / team
	* Projects rolled up into larger programs/initiatives
	* Matrix reporting structures