package org.directoryx.web;

        import org.apache.log4j.Logger;
        import org.directoryx.NeoTestApplication;
        import org.directoryx.domain.Person;
        import org.directoryx.service.PersonService;
        import org.directoryx.service.Service;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.PathVariable;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RestController;
        import org.springframework.web.servlet.ModelAndView;

        import java.util.ArrayList;
        import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping(value = "/api")
public class ApiController extends Controller<Person> {

    static Logger log = Logger.getLogger(NeoTestApplication.class.getName());


    @Autowired
    private PersonService personService;

    @Override
    public Service<Person> getService() {
        return personService;
    }

    @RequestMapping("/people/search/{term}")
    public ArrayList<Person> search(@PathVariable(value = "term") String term) {
        return personService.findByFirstName(term+".*");
    }

}