package org.directoryx.web;

import org.apache.log4j.Logger;
import org.directoryx.NeoTestApplication;
import org.directoryx.domain.Person;
import org.directoryx.service.PersonService;
import org.directoryx.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by iatracey on 6/10/15.
 */
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/people")
public class PersonController extends Controller<Person> {

    static Logger log = Logger.getLogger(NeoTestApplication.class.getName());


    @Autowired
    private PersonService personService;

    @Override
    public Service<Person> getService() {
        return personService;
    }

    @RequestMapping("/person/{userId}")
    public Person getPersonByUserId(@PathVariable(value = "userId") String userId,
                                    final HttpServletResponse response) {
        System.out.println("looking for " + userId + " ...");
        setHeaders(response);
        Person p = personService.findByUserId(userId);
        p.toString();
        return p;
    }


    @RequestMapping("/profile/{userId}")
    public ModelAndView profile(@PathVariable(value = "userId") String userId,
                                final HttpServletResponse response) {
        setHeaders(response);
        Person p = personService.findByUserId(userId);
        ArrayList<Person> reportees = personService.getReportees(userId);
        ArrayList<Person> managers = personService.getManagers(userId);
        ModelAndView mav = new ModelAndView();

//        if(people.size() > 2) {
//            mav.addObject("reporting-chain", people);
//        } else {
//            mav.addObject("reporting-chain-snapshot", people);
//        }

        mav.setViewName("profile");
        Collections.reverse(managers);
        mav.addObject("managers", managers);
        mav.addObject("person", p);
        mav.addObject("reportees", reportees);
        return mav;
    }

    @RequestMapping("/all")
    public Iterable<Person> profile() {
        return personService.findAll();
    }

    @RequestMapping("/reportees/{userId}")
    public Iterable<Person> getManagerByUserId(@PathVariable(value = "userId") String userId) {
        return personService.getReportees(userId);
    }

    @RequestMapping("/search/{term}")
    public ModelAndView search(@PathVariable(value = "term") String term) {
        ArrayList<Person> people = personService.findByFirstName(term+".*");

        ModelAndView mav = new ModelAndView();
        mav.setViewName("search");
        mav.addObject("people", people);

        return mav;
    }
}


//
//

//

//


//        @RequestMapping(value = "/search/{searchTerm}", method = RequestMethod.GET)
//        public Iterable<Person> search(@PathVariable String searchTerm, final HttpServletResponse response) {
//            setHeaders(response);
//            return search(searchTerm);
//        }
//
//        public Iterable<Person> search(String searchTerm) {
//            Iterable<Person> entity = getService().search(searchTerm);
//            if (entity != null) {
//                System.out.println("from OGM: " + entity);
//                return entity;
//            }
//            throw new NotFoundException();
//        }


//        @RequestMapping("/people/{firstName}")
//        public ArrayList<Person> searchPeopleByFirstName(@PathVariable(value = "firstName") String firstName) {
//        ArrayList<Person> people;
//        Transaction tx = graphDatabase.beginTx();
//        try {
//            log.info("Searching Repo for first name...");
//            people = personRepository.findByExactFirstName(firstName);
//            //people = personRepository.searchByFirstName(firstName + ".*");
//            tx.success();
//        } finally {
//            tx.close();
//        }
//        if(people.size() == 0) {
//            log.info("people not found.");
//        } else {
//            log.info(people.size() +" people found.");
//        }
//
//        return people;
//    }





//
//    @RequestMapping("/data/load")
//    public void loadData() {
//        log.info("Begin Transaction...");
//        Transaction tx = graphDatabase.beginTx();
//        try {
//            log.info("Deleting Repo Data...");
//            personRepository.deleteData();
//            log.info("Loading Data Into Repo...");
//            personRepository.loadData();
//            log.info("Done");
//            log.info("Transaction Success!");
//            tx.success();
//        } finally {
//            tx.close();
//        }
//        log.info("Begin Transaction...");
//                tx = graphDatabase.beginTx();
//        try {
//            log.info("Creating userId index...");
//            personRepository.createUserIdIndex();
//            log.info("Done");
//            log.info("Creating managerId index...");
//            personRepository.createManagerIdIndex();
//            log.info("Done");
//            log.info("Creating Index...");
//            personRepository.createFirstNameIndex();
//            log.info("Done");
//            tx.success();
//        } finally {
//            tx.close();
//        }
//        log.info("Begin Transaction...");
//            tx = graphDatabase.beginTx();
//        try {
//            log.info("Making Relationships in Repo...");
//            personRepository.makeRelationships();
//            log.info("Done");
//            tx.success();
//        } finally {
//            tx.close();
//        }
//    }