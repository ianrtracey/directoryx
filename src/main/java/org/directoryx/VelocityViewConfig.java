package org.directoryx;

import org.springframework.boot.autoconfigure.velocity.VelocityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.velocity.VelocityLayoutViewResolver;

@Configuration
public class VelocityViewConfig extends VelocityAutoConfiguration.VelocityWebConfiguration {

    @Bean(name = "velocityViewResolver")
    public VelocityLayoutViewResolver velocityViewResolver() {
        VelocityLayoutViewResolver resolver = new VelocityLayoutViewResolver();
        this.properties.applyToViewResolver(resolver);
        resolver.setLayoutUrl("layout/default.vm");
        return resolver;
    }
}
