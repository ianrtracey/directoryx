package org.directoryx.service;

import org.directoryx.domain.Person;

import java.util.ArrayList;

public interface PersonService extends Service<Person>{

    public Person findByUserId(String userId);

//    public ArrayList<Person> getReportingStructure(String userId);

    public ArrayList<Person> findByFirstName(String firstName);
    public ArrayList<Person> getReportees(String userId);
    public ArrayList<Person> getManagers(String userId);


}
